# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# SPDX-FileCopyrightText: 2012, 2019, 2021, 2022, 2023, 2024 Stefan Asserhäll <stefan.asserhall@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-02 00:37+0000\n"
"PO-Revision-Date: 2024-06-21 07:41+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@gmail.com>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.5\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stefan Asserhäll"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "stefan.asserhall@gmail.com"

#: InternalCollections.cpp:32
#, kde-format
msgid "Sasquatch"
msgstr "Sasquatch"

#: InternalCollections.cpp:36
#, kde-format
msgid "Mas Sasquatch"
msgstr "Mas Sasquatch"

#: InternalCollections.cpp:40
#, kde-format
msgid "Sasquatch III"
msgstr "Sasquatch III"

#: InternalCollections.cpp:44
#, kde-format
msgid "Microban (easy)"
msgstr "Microban (enkel)"

#: InternalCollections.cpp:48
#, kde-format
msgid "Sasquatch IV"
msgstr "Sasquatch IV"

#: main.cpp:26
#, kde-format
msgid "Skladnik"
msgstr "Skladnik"

#: main.cpp:28
#, kde-format
msgid "The japanese warehouse keeper game"
msgstr "Det japanska lagerarbetarspelet"

#: main.cpp:30
#, kde-format
msgid ""
"(c) 1998 Anders Widell <awl@hem.passagen.se>\n"
"(c) 2012 Lukasz Kalamlacki"
msgstr ""
"© 1998 Anders Widell <awl@hem.passagen.se>\n"
"© 2012 Lukasz Kalamlacki"

#: main.cpp:33
#, kde-format
msgid "Shlomi Fish"
msgstr "Shlomi Fish"

#: main.cpp:34
#, kde-format
msgid "For porting to Qt5/KF5 and doing other cleanups"
msgstr "För konvertering till Qt5/KF5 och andan städning."

#: main.cpp:37
#, kde-format
msgid "Lukasz Kalamlacki"
msgstr "Lukasz Kalamlacki"

#: main.cpp:38
#, kde-format
msgid "For rewriting the original ksokoban game from kde3 to kde4"
msgstr ""
"För omskrivning av det ursprungliga Ksokoban-spelet från kde3 till kde4"

#: main.cpp:41
#, kde-format
msgid "Anders Widell"
msgstr "Anders Widell"

#: main.cpp:42
#, kde-format
msgid "For writing the original ksokoban game"
msgstr "För att ha skrivit det ursprungliga Ksokoban-spelet"

#: main.cpp:45
#, kde-format
msgid "David W. Skinner"
msgstr "David W. Skinner"

#: main.cpp:46
#, kde-format
msgid "For contributing the Sokoban levels included in this game"
msgstr "För bidrag av Sokoban-nivåerna som ingår i detta spel"

#: main.cpp:57
#, kde-format
msgid "Level collection file to load."
msgstr "Nivåsamlingsfil att läsa in."

#: MainWindow.cpp:38
#, kde-format
msgctxt "@title:menu"
msgid "Level Collection"
msgstr "Nivåsamling"

#: MainWindow.cpp:97
#, kde-format
msgctxt "@action"
msgid "Load Levels…"
msgstr "Läs in nivåer…"

#: MainWindow.cpp:101
#, kde-format
msgctxt "@action"
msgid "Next Level"
msgstr "Nästa nivå"

#: MainWindow.cpp:106
#, kde-format
msgctxt "@action"
msgid "Previous Level"
msgstr "Föregående nivå"

#: MainWindow.cpp:111
#, kde-format
msgctxt "@action"
msgid "Restart Level"
msgstr "Starta om nivå"

#: MainWindow.cpp:124
#, kde-format
msgctxt "@item animation speed"
msgid "Slow"
msgstr "Långsam"

#: MainWindow.cpp:129
#, kde-format
msgctxt "@item animation speed"
msgid "Medium"
msgstr "Normal"

#: MainWindow.cpp:134
#, kde-format
msgctxt "@item animation speed"
msgid "Fast"
msgstr "Snabb"

#: MainWindow.cpp:139
#, kde-format
msgctxt "@item animation speed"
msgid "Off"
msgstr "Av"

#: MainWindow.cpp:179 MainWindow.cpp:193
#, kde-format
msgctxt "@item bookmark entry"
msgid "(unused)"
msgstr "(oanvänd)"

#: MainWindow.cpp:229
#, kde-format
msgctxt "@item bookmark entry"
msgid "(invalid)"
msgstr "(ogiltig)"

#: MainWindow.cpp:288
#, kde-format
msgctxt "@title:window"
msgid "Load Levels from a File"
msgstr "Läs in nivåer från en fil"

#: MainWindow.cpp:323
#, kde-format
msgctxt "@info"
msgid "No levels found in file."
msgstr "Inga nivåer hittades i filen."

#: PlayField.cpp:91
#, kde-format
msgctxt "@label"
msgid "Level:"
msgstr "Nivå:"

#: PlayField.cpp:100
#, kde-format
msgctxt "@label"
msgid "Steps:"
msgstr "Steg:"

#: PlayField.cpp:109
#, kde-format
msgctxt "@label"
msgid "Pushes:"
msgstr "Knuffar:"

#: PlayField.cpp:270
#, kde-format
msgctxt "@info"
msgid "Level completed."
msgstr "Nivå färdig."

#: PlayField.cpp:582
#, kde-format
msgid "This is the last level in the current collection."
msgstr "Det här är sista nivån i den aktuella samlingen."

#: PlayField.cpp:586
#, kde-format
msgid "You have not completed this level yet."
msgstr "Du har inte gjort färdig nivån ännu."

#: PlayField.cpp:598
#, kde-format
msgid "This is the first level in the current collection."
msgstr "Det här är första nivån i den aktuella samlingen."

#: PlayField.cpp:674
#, kde-format
msgid "Bookmarks for external levels is not implemented yet."
msgstr "Tyvärr är inte bokmärken för externa nivåer implementerade ännu."

#: PlayField.cpp:698
#, kde-format
msgid "This level is broken."
msgstr "Nivån är felaktig."

#. i18n: ectx: Menu (game)
#: skladnikui.rc:6
#, kde-format
msgid "&Game"
msgstr "S&pel"

#. i18n: ectx: Menu (animation_speed)
#: skladnikui.rc:20
#, kde-format
msgid "&Animation"
msgstr "Ani&mering"

#. i18n: ectx: Menu (bookmarks_menu)
#: skladnikui.rc:27
#, kde-format
msgid "&Bookmarks"
msgstr "&Bokmärken"

#. i18n: ectx: Menu (set_bookmarks)
#: skladnikui.rc:29
#, kde-format
msgid "&Set Bookmark"
msgstr "&Nytt bokmärke"

#. i18n: ectx: Menu (goto_bookmarks)
#: skladnikui.rc:42
#, kde-format
msgid "&Go to Bookmark"
msgstr "&Gå till bokmärke"

#~ msgid "KSokoban"
#~ msgstr "Ksokoban"

#~ msgid "[file]"
#~ msgstr "[fil]"

#~ msgid "&Undo"
#~ msgstr "Å&ngra"

#~ msgid "&Redo"
#~ msgstr "&Gör om"

#~ msgid "&Quit"
#~ msgstr "&Avsluta"

#~ msgid "Sasquatch V"
#~ msgstr "Sasquatch V"

#~ msgid "Mas Microban (easy)"
#~ msgstr "Mas Microban (enkelt)"

#~ msgid "Sasquatch VI"
#~ msgstr "Sasquatch VI"

#~ msgid "LOMA"
#~ msgstr "LOMA"

#~ msgid "Sasquatch VII"
#~ msgstr "Sasquatch VII"

#~ msgid "(c) 1998-2001  Anders Widell"
#~ msgstr "© 1998-2001  Anders Widell"
